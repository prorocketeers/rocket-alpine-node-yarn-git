FROM node:alpine

WORKDIR app

RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh
RUN npm update npm -g
