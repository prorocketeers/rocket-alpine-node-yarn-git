# rocket-alpine-node-yarn-git 
Minimal Docker image for running Node.js, git and Yarn, built on Alpine Linux.

Available from the the Docker Hub registry as [https://hub.docker.com/r/prorocketeers/rocket-alpine-node-yarn-git/](https://hub.docker.com/r/prorocketeers/rocket-alpine-node-yarn-git/)

## Versions

- `latest`, - 67.5 MB

## Requirements

* `build.sh` - script that is executed upon building the image. This is typically where you want to run `npm install` and perform additional cleanup
* `up.sh` - script that is executed upon launching the container. Here you typically launch your Node.js application, for instance `node server`
